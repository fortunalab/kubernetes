# Accessing BCB lab Kubernetes Cluster with LENS
> provided by: [Raúl Ortega](https://gitlab.com/raul.ortega)

![LENS](lens_mirantis.png)

## Objective

This tutorial will guide you through adding the [BCB lab](https://bcb.ebd.csic.es) Kubernetes cluster to **LENS** using a configuration file that you received via email. We will also explore how to monitor your pods, jobs, resource quotas, and more using **LENS**.

## Requirements

- **LENS** installed on your machine. You can download it from [here](https://k8slens.dev/).
- The **Kubernetes config file** provided by BCB lab (sent via email or downloaded from the pod gateway).
- Access to the **BCB lab cluster** via the correct IP and port (in this example, we’ll use **192.168.0.24** as the host and **16443** as the API port).
- Modify the **IP** and **port** in the config file accordingly before using it.

## Step 1: Modifying the Configuration File
**Note:** When you receive the configuration file from the BCB lab, it won't need to be updated with the correct **IP address** and **port** for the cluster’s API server.

However, if you didn't receive the configuration file from the BCB lab, you can download it from the SFTP server:

1. **Access your SFT server** as explained [here](https://gitlab.com/ebd-bcb/kubernetes/-/blob/master/docs/sftp/readme.md).

2. **Show hidden files**

   The .kube folder will be shown together with bcb.

   ![Show hidden files](lens_show_hidden_files.png)
  
3. **Navigate to .kube folder** and **download the config** file.

4. **Locate** the section that contains the cluster address. It will look something like this:

    ```yaml
    clusters:
    - cluster:
        server: https://<ip>:<port>
    ```

5. **Change the IP and port** to match the following:

    ```yaml
    clusters:
    - cluster:
        server: https://192.168.0.24:16443
    ```
    ```
    
5. **Save the file** and ensure it's ready to be used.
  
## Step 2: Adding the Cluster to LENS

On GNU/Linux systems, the common practice is to **place the config file** in the **~/.kube/** directory, and LENS will automatically detect and add it to the catalog.

As an alternative that can be used on all systems:

1. **Open LENS** on your machine.

2. In the top menu, click on **"File" > "Add Cluster"**.

3. Choose the option **"Add Cluster from Kubeconfig"**.

4. **Copy** the content of the config file you just updated on Step 1

5. **Paste** the content inside the window.

   ![Select Config](lens_paste_config.png)

6. **Add the cluster** by clicking on **"Add cluster"** button.

   You should now see the cluster listed in the catalog as follows:

   ![Cluster Added](lens_cluster_added.png)

## Step 3: Viewing Your Resources in LENS

Once you have successfully added the BCB lab cluster, you will have access to various resources in your namespace.

### 1. **Viewing Pods in Your Namespace**

- Navigate to the **"Workloads"** section on the left panel.
- Click on **"Pods"**.
  
   ![View Pods](lens_workload_pods.png)

- You will see a list of all pods in your namespace.

### 2. **Viewing Jobs**

- Go to **"Workloads"**.
- Click on **"Jobs"**.
- This will show you all the **batch jobs** running in your namespace, that should be empty for the first time.

### 3. **Checking Resource Quotas**

- Go to the **"Namespaces"** section.
- Click on your **namespace**.

   ![Resource Quotas](lens_resource_quota.png)

- Under the namespace view, you will find the **"Resource Quotas"** tab, where you can check the quotas that are available to you.

   ![Resource Quota Details](lens_resource_quota_details.png)

## Additional Permissions

As a user, you also have access to a broader set of resources. While your most common interactions will involve viewing your **pods**, **jobs**, and **resource quotas**, you can also access the following resources based on your role's permissions:

- **Pods**, **events**, **persistent volumes**, **services**, and **secrets**: These can be listed, viewed, and managed in their respective sections within **LENS**.
- **Pods exec**: You can open a terminal session in a running pod directly through **LENS**.
- **Pods log**: You can view the logs of a pod to debug or monitor its performance.
- **ConfigMaps and Endpoints**: Accessible via the **Configuration** section in LENS.
  
For a full exploration of your access rights, you can view the **Roles** and **RoleBindings** associated with your account in the **Access Control** section of LENS.

### Service Account Permissions (Overview)

Your service account is allowed to:

- **List** and **get namespaces** and **resource quotas**.
- **Full access** to **pods**, **jobs**, **config maps**, and **persistent volumes** (creating, updating, deleting).
- **View storage resources** such as **storage classes** and **CSINodes**.

However, it is recommended that you stick to working within your **namespace** unless directed otherwise.

## Conclusion

This tutorial covered how to add the **BCB lab Kubernetes cluster** to LENS using a config file. You now know how to explore your **pods**, **jobs**, and **resource quotas**. LENS offers a user-friendly interface to manage your Kubernetes resources, and with the permissions provided to your service account, you can handle a variety of tasks within your namespace.

If you encounter any issues or need further assistance, feel free to reach out us.
