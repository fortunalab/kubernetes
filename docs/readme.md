# Working on our Kubernetes cluster
> provided by: [Raúl Ortega](https://gitlab.com/raul.ortega)

Here, we provide a set of tutorials on accessing the BCB Kubernetes cluster. You will learn how to upload files, connect to the cluster via ssh, launch jobs in Kubernetes, monitor execution, retrieve outputs, and clean up after finishing.

---

#### [Connecting to an SFTP Server.](sftp/readme.md)
> uploading and retrieving files to/from the cluster through *sftp*.
#### [Accessing a Host via SSH on a Specific Port.](ssh/readme.md)
>  accessing the cluster via *ssh* on a specific port.
#### [Operating the BCB lab Cluster Using kubectl.](kubectl/readme.md)
> installing and configuring *kubectl* for cluster resource management.
#### [Accessing BCB lab Kubernetes Cluster with LENS.](lens/readme.md)
> exploring pods, jobs, and resource quotas using the *LENS* interface.
#### [How to Connect to RStudio Server.](rstudio/readme.md)
> connecting to *RStudio* server via web browser.
#### [Running Multiple Replicas of an R Script using Kubernetes Jobs.](job_r/readme.md)
> running multiple replicas of an R script as *Kubernetes* jobs.

---
